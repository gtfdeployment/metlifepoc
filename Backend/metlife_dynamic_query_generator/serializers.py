from rest_framework import serializers
from .models import Company, Absence, Product, Co_Ab_ci_Pr_Map, Question, Question_Pr_Map, Answer


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ["name", "id"]


class AbsenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Absence
        fields = ["reason"]


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ["name"]


class Co_Ab_ci_Pr_MapSerializer(serializers.ModelSerializer):
    # prs = serializers.StringRelatedField(many=True)

    class Meta:
        model = Co_Ab_ci_Pr_Map
        fields = ["pr_id"]


class Co_Ab_ci_Pr_MapSerializer_for_abs(serializers.ModelSerializer):
    company = serializers.StringRelatedField(many=True)

    # co_id = serializers.StringRelatedField(many=True)
    # pr_id = serializers.StringRelatedField(many=True)

    class Meta:
        model = Company
        fields = ["company"]


class QuestionSerializer(serializers.ModelSerializer):
    question_values = serializers.JSONField()

    class Meta:
        model = Question
        fields = ["id", "question_str", "is_multivalued", "input_field_type", "is_required", "question_values"]


class Question_Pr_MapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question_Pr_Map
        fields = ["q_id", "pr_id"]


# class ProductSerializer_questions(serializers.ModelSerializer):
#     product_1 = QuestionSerializer(many=True)
#     product_1
#     class Meta:
#         model = Product
#         fields = ["name", "product_1"]


class AnswerSerializer(serializers.ModelSerializer):
    answer_values = serializers.JSONField()

    class Meta:
        model = Answer
        fields = ["claim_id", "answer_values"]
