from django.db import models


# Create your models here.

class Absence(models.Model):
    reason: models.CharField = models.CharField(blank=False, max_length=300)
    created_at: models.DateTimeField = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.reason}"


class Product(models.Model):
    name: models.CharField = models.CharField(blank=False, max_length=300)
    created_at: models.DateTimeField = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}"


class Company(models.Model):
    name: models.CharField = models.CharField(blank=False, max_length=300)
    created_at: models.DateTimeField = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}"


class Co_Ab_ci_Pr_Map(models.Model):
    co_id = models.ForeignKey(Company, related_name="company", on_delete=models.CASCADE)
    ab_id = models.ForeignKey(Absence, related_name="absence", on_delete=models.CASCADE)
    c_and_i: models.CharField = models.CharField(blank=False, max_length=40)
    pr_id = models.ForeignKey(Product, related_name="product", on_delete=models.CASCADE)
    created_at: models.DateTimeField = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"company:{self.co_id} absence:{self.ab_id} c_and_i: {self.c_and_i} product:{self.pr_id}"


class Question(models.Model):
    question_str: models.TextField = models.TextField(blank=False, max_length=600)
    is_multivalued: models.BooleanField = models.BooleanField(blank=False)
    input_field_type: models.CharField = models.CharField(blank=False, max_length=100)
    is_required: models.BooleanField = models.BooleanField(blank=False)
    created_at: models.DateTimeField = models.DateTimeField(auto_now_add=True)
    question_values: models.JSONField = models.JSONField(blank=True)

    def __str__(self):
        # return f"Question : {self.question_str}, input_type : {self.input_field_type}"
        return f"Question : {self.question_str}"


class Question_Pr_Map(models.Model):
    q_id = models.ForeignKey(Question, related_name="question", on_delete=models.CASCADE)
    pr_id = models.ForeignKey(Product, related_name="product_1", on_delete=models.CASCADE)
    created_at: models.DateTimeField = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"q_id : {self.q_id}, pr_id : {self.pr_id}"


class Answer(models.Model):
    # q_id = models.ForeignKey(Question, related_name="question", on_delete=models.CASCADE)
    # answer_value: models.CharField() = models.CharField(max_length=200, blank=False)
    created_at: models.DateTimeField = models.DateTimeField(auto_now_add=True)
    claim_id: models.CharField = models.CharField(blank=False, max_length=100)
    # emailId: models.EmailField() = models.EmailField(blank=False)
    answer_values: models.JSONField = models.JSONField(blank=True)

    # q_id: models.CharField() = models.CharField(max_length=200, blank=False, unique=True)
    def __str__(self):
        return f"{self.claim_id}"
