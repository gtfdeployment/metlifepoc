from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .models import Company, Co_Ab_ci_Pr_Map, Question_Pr_Map, Product, Question
from .serializers import CompanySerializer, QuestionSerializer, AnswerSerializer, ProductSerializer
from logging import getLogger
import uuid

# Create your views here.
root_logger = getLogger(__name__)


class Company_res(APIView):

    def get(self, request):
        """
        To get list of companies
        """
        try:
            companies = Company.objects.all()
        except Exception as ex:
            root_logger.error(ex)
            root_logger.debug("UNABLE TO GET COMPANIES")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        companies = CompanySerializer(companies, many=True)
        c_list = []
        for company in companies.data:
            c_list.append(company["name"])
        r = {
            "companies": c_list,
        }
        return Response(r, status=status.HTTP_200_OK)


class AbsenceRes(APIView):

    def get(self, request):
        try:
            company = request.query_params['company']
            full = Co_Ab_ci_Pr_Map.objects.filter(co_id__name=company).select_related('ab_id')
        except Exception as ex:
            root_logger.error(ex)
            root_logger.debug("UNABLE TO GET ABSENCES")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        reasons = []
        for element in full:
            reasons.append(str(element.ab_id))
        data = {
            "reasons": set(reasons)
        }
        return Response(data, status=status.HTTP_200_OK)


class QuestionsRes(APIView):

    def get(self, request):
        try:
            company = request.query_params['company']
            reason_ab = request.query_params['reason']
            c_or_i = request.query_params['c_i']
            pr_ids = Co_Ab_ci_Pr_Map.objects.filter(co_id__name=company, ab_id__reason=reason_ab, c_and_i=c_or_i) \
                .values_list("pr_id", flat=True)
            questions = Question_Pr_Map.objects.filter(pr_id__in=pr_ids).order_by("q_id").select_related("q_id")
        except Exception as ex:
            root_logger.error(ex)
            root_logger.debug("UNABLE TO GET QUESTIONS")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        res = []
        for question in questions:
            res.append(QuestionSerializer(question.q_id).data)
        res = list({element['id']: element for element in res}.values())
        return Response(res, status=status.HTTP_200_OK)

    def post(self, request):
        try:
            payload = request.data
            question = payload["question"]
            product = payload["product"]["name"]
            question = Question.objects.create(**question)
            product = Product.objects.get(name=product)
            Question_Pr_Map.objects.create(q_id=question, pr_id= product)
            return Response(status=status.HTTP_201_CREATED)
        except Exception as ex:
            root_logger.error(ex)
            root_logger.debug(f"ENTERED POST IN QUES_RES API UNABLE TO SAVE POSTED VALUES ")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class AnswerRes(APIView):

    def post(self, request):
        claim_id = uuid.uuid4().hex[:6].upper()
        payload = {
            "claim_id": claim_id,
            "answer_values": request.data
        }
        serializer = AnswerSerializer(data=payload)
        if serializer.is_valid():
            try:
                serializer.save()
                root_logger.debug(f"ENTERED POST IN ANSWER_RES API SAVED POSTED VALUES")
            except Exception as ex:
                root_logger.debug(f"ENTERED POST IN ANSWER_RES API UNABLE TO SAVE POSTED VALUES ")
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            return Response(claim_id, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductsRes(APIView):

    def get(self, request):
        try:
            products = Product.objects.all()
            products = ProductSerializer(products, many=True)
        except Exception as ex:
            root_logger.error(ex)
            root_logger.debug("UNABLE TO GET PRODUCTS")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        pr_list = []
        for product in products.data:
            pr_list.append(product["name"])
        return Response(pr_list, status=status.HTTP_200_OK)
