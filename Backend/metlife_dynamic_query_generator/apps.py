from django.apps import AppConfig


class MetlifeDynamicQueryGeneratorConfig(AppConfig):
    name = 'metlife_dynamic_query_generator'
