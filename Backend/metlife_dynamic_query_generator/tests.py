from rest_framework.test import APITestCase
from rest_framework import status
from .models import Absence, Product, Question, Company, Question_Pr_Map, Co_Ab_ci_Pr_Map


# Create your tests here.

class ProductResTestCase(APITestCase):
    url = "/metlife/claims/products/"

    def test_create_products_and_get(self):
        """
        Ensure we can create absences and get absence
        """
        Product.objects.create(name="STD")
        response = self.client.get(self.url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AnswerResTestCase(APITestCase):
    url = "/metlife/claims/answers/"

    def test_post_answers(self):
        """
        Ensure we can post answers to a question
        """
        data = [
            {
                "id": 1,
                "email": "kalyangande@virtusa.com"
            }
        ]
        response = self.client.post(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class QuestionResAbsenceResTestCase(APITestCase):
    url = "/metlife/claims/questions/"
    question = {
        "question": {
            "question_str": "This is test",
            "is_multivalued": False,
            "input_field_type": "text",
            "is_required": True,
            "question_values": []
        },
        "product": {
            "name": "STD"
        }
    }
    absence_url = "/metlife/claims/absence/"

    def test_post_question(self):
        """
        Ensure we can post answers to a question
        """
        Product.objects.create(name="STD")

        response = self.client.post(self.url, data=self.question, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_question_get_absence(self):
        """
        Ensure we can get questions
        """
        company = Company.objects.create(name="google")
        absence = Absence.objects.create(reason="child_bonding")
        product = Product.objects.create(name="STD")
        question = Question.objects.create(**self.question["question"])
        q_pr_map = Question_Pr_Map.objects.create(q_id=question, pr_id=product)
        co_ab_ci_pr_map = Co_Ab_ci_Pr_Map.objects.create(co_id=company, ab_id=absence, c_and_i="C", pr_id=product)
        response = self.client.get(self.url + "?company=google&reason=child_bonding&c_i=C", format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(self.absence_url+"?company=google")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
