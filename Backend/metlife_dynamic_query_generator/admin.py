from django.contrib import admin
from .models import Company, Absence, Product, Co_Ab_ci_Pr_Map, Question, Question_Pr_Map, Answer
from import_export import resources
from import_export.admin import ImportExportModelAdmin


# Register your models here.

class CompanyResource(resources.ModelResource):
    class Meta:
        model = Company


class AbsenceResource(resources.ModelResource):
    class Meta:
        model = Absence


class ProductResource(resources.ModelResource):
    class Meta:
        model = Product


class Co_Ab_ci_Pr_MapResource(resources.ModelResource):
    class Meta:
        model = Co_Ab_ci_Pr_Map


class QuestionResource(resources.ModelResource):
    class Meta:
        model = Question


class Question_Pr_MapResource(resources.ModelResource):
    class Meta:
        model = Question_Pr_Map


class CompanyAdmin(ImportExportModelAdmin):
    resource_class = CompanyResource


class AbsenceAdmin(ImportExportModelAdmin):
    resource_class = AbsenceResource


class ProductAdmin(ImportExportModelAdmin):
    resource_class = ProductResource


class Co_Ab_ci_Pr_MapAdmin(ImportExportModelAdmin):
    resource_class = Co_Ab_ci_Pr_MapResource


class QuestionAdmin(ImportExportModelAdmin):
    resource_class = QuestionResource


class Question_Pr_MapAdmin(ImportExportModelAdmin):
    resource_class = Question_Pr_MapResource


admin.site.register(Company, CompanyAdmin)
admin.site.register(Absence, AbsenceAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Co_Ab_ci_Pr_Map, Co_Ab_ci_Pr_MapAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Question_Pr_Map, Question_Pr_MapAdmin)
admin.site.register(Answer)