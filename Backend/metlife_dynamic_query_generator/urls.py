from django.urls import path

from . import views

urlpatterns = [
    path('company/', views.Company_res.as_view(), name="company"),
    path('absence/', views.AbsenceRes.as_view(), name="absence"),
    path('questions/', views.QuestionsRes.as_view(), name="questions"),
    path('answers/', views.AnswerRes.as_view(), name="answers"),
    path('products/', views.ProductsRes.as_view(), name="products"),
]