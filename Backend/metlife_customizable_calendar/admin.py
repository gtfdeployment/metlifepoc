from django.contrib import admin
from .models import Holiday
from import_export import resources
from import_export.admin import ImportExportModelAdmin


# Register your models here.

class HolidayResource(resources.ModelResource):
    class Meta:
        model = Holiday


class HolidayAdmin(ImportExportModelAdmin):
    resource_class = HolidayResource


admin.site.register(Holiday, HolidayAdmin)
