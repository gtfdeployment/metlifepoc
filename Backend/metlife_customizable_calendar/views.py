from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from logging import getLogger
import holidays
from .models import Holiday
from .serializers import HolidaySerializer

root_logger = getLogger(__name__)


def get_hdys(country, year):
    hldy_str = f"holidays.{country}(years={year})"
    hldys = eval(hldy_str)
    dates = []
    for date, name in sorted(hldys.items()):
        dates.append(date)
    return dates


class CalendarRes(APIView):

    def get(self, request):
        try:
            year = request.query_params['year']
            country = request.query_params['country']
            dates = get_hdys(country, year)
            return Response(dates, status=status.HTTP_200_OK)
        except Exception as ex:
            root_logger.error(ex)
            root_logger.debug("UNABLE TO GET HOLIDAYS")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CalendarAdminRes(APIView):

    def get(self, request):
        try:
            year = request.query_params['year']
            country = request.query_params['country']
            hldys = Holiday.objects.filter(country=country, hldy_date__year=int(year))
            root_logger.debug(f"ENTERED GET IN CALENDAR_ADMIN_RES API GET SUCCESS")
        except Exception as ex:
            root_logger.error(ex)
            root_logger.debug(f"ENTERED GET IN CALENDAR_ADMIN_RES API GET FAIL")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        hldys = HolidaySerializer(hldys, many=True)
        dates = get_hdys(country, year)
        for hldy in hldys.data:
            dates.append(hldy["hldy_date"])
        return Response(dates, status=status.HTTP_200_OK)

    def post(self, request):
        payload = request.data
        serializer = HolidaySerializer(data=payload, many=True)
        if serializer.is_valid():
            try:
                serializer.save()
                root_logger.debug(f"ENTERED POST IN CALENDAR_ADMIN_RES API SAVED POSTED VALUES")
            except Exception as ex:
                root_logger.error(ex)
                root_logger.debug(f"ENTERED POST IN CALENDAR_ADMIN_RES API UNABLE TO SAVE POSTED VALUES")
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
