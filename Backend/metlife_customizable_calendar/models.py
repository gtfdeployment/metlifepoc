from django.db import models


# Create your models here.
class Holiday(models.Model):
    description: models.CharField = models.CharField(blank=False, max_length=100)
    hldy_date: models.DateField = models.DateField()
    country: models.CharField = models.CharField(blank=True, max_length=100)
    created_at: models.DateTimeField = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.hldy_date}"
