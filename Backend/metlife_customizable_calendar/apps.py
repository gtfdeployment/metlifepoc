from django.apps import AppConfig


class MetlifeCustomizableCalendarConfig(AppConfig):
    name = 'metlife_customizable_calendar'
