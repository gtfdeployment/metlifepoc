# from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework import status


# Create your tests here.

class CalendarAdminResTestCase(APITestCase):
    url = '/metlife/holidays/'

    def test_post_holidays(self):
        """
        Ensure we can post dates for holidays and get holidays list
        """
        data = [{
            "description": "Test",
            "hldy_date": "2020-12-29",
            "country": "UnitedStates"
        },
            {
                "description": "Test",
                "hldy_date": "2020-12-30",
                "country": "UnitedStates"
            }]
        response = self.client.post(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get(self.url + '?year=2020&country=UnitedStates', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
