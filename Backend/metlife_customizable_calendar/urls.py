from django.urls import path

from . import views

urlpatterns = [
    path('', views.CalendarAdminRes.as_view(), name='admin_calendar'),
    path('default', views.CalendarRes.as_view(), name='admin_calendar_def')
]
