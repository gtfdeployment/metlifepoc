from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from logging import getLogger

root_logger = getLogger(__name__)
ddg_ui_logger = getLogger('ddg-ui')



class UILogging(APIView):

    def post(self, request):
        email = None
        if email is not None:
            root_logger.info(f"ENTERED GET IN UILogging API BY EMAIL : {email}")
            level = 'INFO'
            try:
                ddg_ui_logger.info(f"{request.data['timestamp']} "
                                    f"{request.data['fileName']}  {request.data['lineNumber']} {level} "
                                    f"{request.data['message']} {email}  ")
                root_logger.info(f"SAVED LOGS IN UILogging API BY EMAIL : {email}")
                return Response(status=status.HTTP_201_CREATED)
            except Exception as ex:
                root_logger.error(ex)
                return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_401_UNAUTHORIZED)
