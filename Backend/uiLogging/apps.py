from django.apps import AppConfig


class UiloggingConfig(AppConfig):
    name = 'uiLogging'
