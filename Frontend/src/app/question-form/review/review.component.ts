/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  ChangeDetectorRef,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GlobalService } from 'src/app/global.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css'],
})
export class ReviewComponent implements OnInit, OnChanges {
  @Input() formGroup: FormGroup;
  @Input() fields: any;
  @Input() formName;

  dates: any = [];
  myFilter = (d: Date | null): boolean => {
    const day = (d || new Date()).getDay();
    const da = this.datePipe.transform(d, 'yyyy-MM-dd');
	console.log('Date :' + day  );
	console.log('Date :'  + da  );
	console.log();
    return day !== 0 && day !== 6 && !this.dates.includes(da);
  };
  constructor(
    public service: GlobalService,
    private cd: ChangeDetectorRef,
    public datePipe: DatePipe
  ) {}
  isReadOnly = true;
  ngOnInit(): void {
    if (this.formName == 'Date of Absence') {
      this.service.getHolidays('UnitedStates', '2020').subscribe((res) => {
        this.dates = res;
      });
    } else {
      this.myFilter = (d: Date | null): boolean => {
        const day = (d || new Date()).getDay();
        const da = this.datePipe.transform(d, 'yyyy-MM-dd');
        return true;
      };
    }
  }
  change(i) {
    const id = 'matIcon' + i;
    if (this.fields[i]['isReadOnly']) {
      this.fields[i]['isReadOnly'] = false;
      document.getElementById(id).innerText = 'check_circle';
    } else {
      this.fields[i]['isReadOnly'] = true;
      document.getElementById(id).innerText = 'mode_edit';
      this.service.emitFormgroup(this.formGroup);
    }
  }
  changeMode() {
    this.isReadOnly = false;
  }
  save() {
    this.fields.forEach((item) => {
      if (item.input_field_type == 'calendar') {
        item['answer'] = this.datePipe.transform(
          this.formGroup.get(item.question_str).value,
          'yyyy-MM-dd'
        );
      } else {
        item['answer'] = this.formGroup.get(item.question_str).value;
      }
    });
    if (this.formName == 'Reason for Absence') {
      this.service.emitStep(0);
    }
    this.isReadOnly = true;
  }
  ngOnChanges() {
    this.cd.detectChanges();
  }
}

// if(this.formName=='Personal Details'){
//   this.service.emitFormgroup({'name':'Personal Details','value':this.formGroup});
// } else if(this.formName=='Date of Absence'){
//   this.service.emitFormgroup({'name':'Date of Absence','value':this.formGroup});
// } else if(this.formName=='Reason for Absence'){
//   this.service.emitFormgroup({'name':'Reason for Absence','value':this.formGroup});
// }
