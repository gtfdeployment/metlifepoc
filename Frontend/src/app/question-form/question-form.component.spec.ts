/* eslint-disable @typescript-eslint/no-unused-vars */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionFormComponent } from './question-form.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('QuestionFormComponent', () => {
  let component: QuestionFormComponent;
  let fixture: ComponentFixture<QuestionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuestionFormComponent],
      imports: [HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionFormComponent);
    component = fixture.componentInstance;
    const fields = [
      {
        id: 1,
        question_str: 'Employee',
        input_field_type: 'text',
        is_required: 'true',
        maxLength: 10,
        question_values: [],
      },
    ];

    // simulate the parent setting the input property with that hero
    component.fields = fields;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have input field Employee', () => {
    const inputElement: HTMLElement = fixture.nativeElement;
    const p = inputElement.querySelector('mat-label');
    expect(p.textContent).toEqual('Employee');
  });
});
