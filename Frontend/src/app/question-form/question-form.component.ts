/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.css'],
})
export class QuestionFormComponent implements OnInit {
  constructor(public service: GlobalService, private cd: ChangeDetectorRef) {}
  @Input() formGroup: FormGroup;
  @Input() fields: any;
  countryList = ['India', 'US', 'China', 'Australia', 'Russia'];
  ngOnInit(): void {}
  ngOnChanges() {
    this.cd.detectChanges();
  }
}
