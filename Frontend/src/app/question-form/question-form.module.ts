import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuestionFormRoutingModule } from './question-form-routing.module';
import { QuestionFormComponent } from './question-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import { ReviewComponent } from './review/review.component';
import { MatButtonModule } from '@angular/material/button';
import { GlobalService } from '../global.service';
@NgModule({
  declarations: [QuestionFormComponent, ReviewComponent],
  imports: [
    CommonModule,
    QuestionFormRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatButtonModule
  ],
  exports:[
  QuestionFormComponent,
  ReviewComponent],
   providers:[GlobalService,DatePipe]
})
export class QuestionFormModule { }
