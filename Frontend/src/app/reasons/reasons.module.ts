import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { ReasonsRoutingModule } from './reasons-routing.module';
import { ReasonsComponent } from './reasons.component';
import {MatRadioModule} from '@angular/material/radio';
import { GlobalService } from '../global.service';

@NgModule({
  declarations: [ReasonsComponent],
  imports: [
    CommonModule,
    ReasonsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule
  ],
  providers:[GlobalService],
  exports: [ReasonsComponent]
})
export class ReasonsModule { }
