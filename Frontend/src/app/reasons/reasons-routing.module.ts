import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReasonsComponent } from './reasons.component';

const routes: Routes = [{ path: '', component: ReasonsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReasonsRoutingModule { }
