/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-empty-function */
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  ChangeDetectorRef,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-reasons',
  templateUrl: './reasons.component.html',
  styleUrls: ['./reasons.component.css'],
})
export class ReasonsComponent implements OnInit, OnChanges {
  @Input() reasonFormGroup: FormGroup;
  @Input() reasonInput;
  constructor(public service: GlobalService, private cd: ChangeDetectorRef) {}
  reason;
  type;
  reasonList = [
    'Pregency',
    'Child Bonding',
    'Care of Family Member',
    'Military Exigency',
    'Care to Service Member',
    'Other',
  ];
  typeList = ['Continous', 'Intermittent'];
  ngOnInit(): void {}
  ngOnChanges() {
    this.reasonList = this.reasonInput;
  }
}
