import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { DateSelectRoutingModule } from './date-select-routing.module';
import { DateSelectComponent } from './date-select.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { GlobalService } from '../global.service';

export const MY_FORMATS = {
  parse: {
      dateInput: 'LL'
  },
  display: {
      dateInput: 'YYYY-MM-DD',
      monthYearLabel: 'YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'YYYY'
  }
};

@NgModule({
  declarations: [DateSelectComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DateSelectRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    MatNativeDateModule
  ],
  exports:[
    DateSelectComponent
  ],
  providers:[GlobalService,DatePipe]
})
export class DateSelectModule { }
