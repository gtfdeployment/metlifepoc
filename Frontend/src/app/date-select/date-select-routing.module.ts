import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DateSelectComponent } from './date-select.component';

const routes: Routes = [{ path: '', component: DateSelectComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DateSelectRoutingModule { }
