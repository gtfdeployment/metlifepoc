/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-date-select',
  templateUrl: './date-select.component.html',
  styleUrls: ['./date-select.component.css'],
})
export class DateSelectComponent implements OnInit {
  @Input() formGroup: FormGroup;
  dates: any = [];
  myFilter = (d: Date | null): boolean => {
    const day = (d || new Date()).getDay();
    const da = this.datePipe.transform(d, 'yyyy-MM-dd');
    return day !== 0 && day !== 6 && !this.dates.includes(da);
  };
  countryList = ['India', 'UnitedStates', 'China', 'Australia', 'Russia'];
  constructor(
    public datePipe: DatePipe,
    public service: GlobalService,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.service.getHolidays('UnitedStates', '2020').subscribe((res) => {
      this.dates = res;
    });
  }
  selectionChange(value) {
    this.service.getHolidays(value, '2020').subscribe((res) => {
      this.dates = res;
    });
  }
}
