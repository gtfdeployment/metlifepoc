/* eslint-disable @typescript-eslint/no-unused-vars */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperContentComponent } from './stepper-content.component';

describe('StepperContentComponent', () => {
  let component: StepperContentComponent;
  let fixture: ComponentFixture<StepperContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StepperContentComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperContentComponent);
    component = fixture.componentInstance;
    component.stepName = 'First Step';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should bind step name', () => {
    const inputElement: HTMLElement = fixture.nativeElement;
    const p = inputElement.querySelector('.heading');
    expect(p.textContent).toEqual(component.stepName);
    component.stepName = 'another';
    fixture.detectChanges();
    expect(p.textContent).toEqual('another');
  });
});
