import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-stepper-content',
  templateUrl: './stepper-content.component.html',
  styleUrls: ['./stepper-content.component.css']
})
export class StepperContentComponent implements OnInit {
  @Input() stepName;
  @Input() src;
  constructor() { }

  ngOnInit(): void {
  }

}
