/* eslint-disable @typescript-eslint/no-unused-vars */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { LandingComponent } from './landing/landing.component';

const routes: Routes = [
  {
    path: 'claim',
    component: MainComponent,
  },
  {
    path: '',
    component: LandingComponent,
  },
  {
    path: 'question-form',
    loadChildren: () =>
      import('./question-form/question-form.module').then(
        (m) => m.QuestionFormModule
      ),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./configure/configure.module').then((m) => m.ConfigureModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
