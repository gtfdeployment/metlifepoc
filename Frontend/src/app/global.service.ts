/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class GlobalService {
  constructor(public httpClient: HttpClient) {
    this.formEmitter = new EventEmitter<any>();
    this.stepEmitter = new EventEmitter<any>();
  }
  public formEmitter: EventEmitter<any>;
  public emitFormgroup(form: any) {
    this.formEmitter.emit(form);
  }
  public stepEmitter: EventEmitter<any>;
  public emitStep(value) {
    this.stepEmitter.emit(value);
  }
  getHolidays(country, year) {
    return this.httpClient
      .get(
        'http://10.62.10.33/metlife/holidays/?year=' +
          year +
          '&country=' +
          country
      )
      .pipe();
  }
  getReasons(company) {
    return this.httpClient
      .get('http://10.62.10.33/metlife/claims/absence?company=' + company)
      .pipe();
  }
  getGeneric(url) {
    return this.httpClient.get(url).pipe();
  }
  getQuestions(reason, type) {
    return this.httpClient
      .get(
        'http://10.62.10.33/metlife/claims/questions?company=google&reason=' +
          reason +
          '&c_i=' +
          type[0]
      )
      .pipe();
  }
  postForm(form: any, url) {
    const header = new HttpHeaders({ 'content-type': 'application/json' });
    return this.httpClient.post(url, form, { headers: header }).pipe();
  }
}
