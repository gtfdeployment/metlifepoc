/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Component, OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  FormGroup,
  FormBuilder,
  Validators,
  NgForm,
  FormGroupDirective,
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { GlobalService } from '../global.service';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-configure',
  templateUrl: './configure.component.html',
  styleUrls: ['./configure.component.css'],
})
export class ConfigureComponent implements OnInit {
  products: any = [];
  leaveFormGroup: FormGroup;
  postFormat = {
    question: {
      question_str: '',
      is_multivalued: false,
      input_field_type: 'text',
      is_required: true,
      question_values: [],
    },
    product: {
      name: 'STD',
    },
  };
  countryList = ['India', 'UnitedStates', 'Germany', 'Australia', 'Russia'];
  inputType = ['text', 'email', 'textarea', 'dropdown', 'radio'];
  constructor(
    private _formBuilder: FormBuilder,
    public datePipe: DatePipe,
    public service: GlobalService,
    private _snackBar: MatSnackBar
  ) {}
  questionFormGroup: FormGroup;
  ngOnInit(): void {
    this.service.getGeneric(environment.products).subscribe((res) => {
      this.products = res;
    });
    this.leaveFormGroup = this._formBuilder.group({
      country: ['', Validators.required],
      description: ['', Validators.required],
      hldy_date: ['', Validators.required],
    });
    this.questionFormGroup = this._formBuilder.group({
      product: ['', Validators.required],
      question_str: ['', Validators.required],
      input_field_type: ['', Validators.required],
      is_multivalued: [false, Validators.required],
      is_required: [false, Validators.required],
    });
  }
  isMulti = false;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  questionValues = [];

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.questionValues.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  remove(values): void {
    const index = this.questionValues.indexOf(values);

    if (index >= 0) {
      this.questionValues.splice(index, 1);
    }
  }
  saveForm(formDirective: FormGroupDirective) {
    if (this.questionFormGroup.valid) {
      this.postFormat.question.question_str = this.questionFormGroup.get(
        'question_str'
      ).value;
      this.postFormat.question.input_field_type = this.questionFormGroup.get(
        'input_field_type'
      ).value;
      this.postFormat.question.is_required = this.questionFormGroup.get(
        'is_required'
      ).value;
      this.postFormat.question.is_multivalued = this.questionFormGroup.get(
        'is_multivalued'
      ).value;
      this.postFormat.question.question_values = this.questionValues;
      this.postFormat.product.name = this.questionFormGroup.get(
        'product'
      ).value;
      console.log('postForm Values', this.postFormat);
      this.service
        .postForm(this.postFormat, environment.addQuestions)
        .subscribe(
          (res) => {
            this._snackBar.open('Added Successfully', 'Success', {
              duration: 5000,
              panelClass: ['notif-success'],
            });
            formDirective.resetForm();
            this.questionValues = [];
            this.questionFormGroup.reset();
            this.isMulti = false;
          },
          (error) => {
            this._snackBar.open('Something went wrong', 'Failed', {
              duration: 2000,
            });
          }
        );
    }
  }
  discard() {
    this.questionValues = [];
    this.questionFormGroup.reset();
  }
  discard1() {
    this.leaveFormGroup.reset();
  }
  saveCalendarForm(formDirective: FormGroupDirective) {
    if (this.leaveFormGroup.valid) {
      const data = [];
      this.leaveFormGroup
        .get('hldy_date')
        .setValue(
          this.datePipe.transform(
            this.leaveFormGroup.get('hldy_date').value,
            'yyyy-MM-dd'
          )
        );
      console.log('form values', this.leaveFormGroup.value);
      data[0] = this.leaveFormGroup.value;
      this.service.postForm(data, environment.postHolidays).subscribe(
        (res) => {
          this._snackBar.open('Added Successfully', 'Success', {
            duration: 5000,
            panelClass: ['notif-success'],
          });
          formDirective.resetForm();
          this.leaveFormGroup.reset();
        },
        (error) => {
          this._snackBar.open('Something went wrong', 'Failed', {
            duration: 2000,
          });
        }
      );
    }
  }
  checkBoxChange(completed: boolean) {
    this.isMulti = completed;
  }
  selectionChange(value) {
    if (value == 'dropdown' || value == 'radio') {
      this.isMulti = true;
    } else {
      this.isMulti = false;
    }
  }
}
