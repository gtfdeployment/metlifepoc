/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import {
  FormGroup,
  Validators,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { GlobalService } from '../global.service';
import { MatStepper } from '@angular/material/stepper';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  reasonFormGroup: FormGroup;
  dateFormGroup: FormGroup;
  detailsFormGroup: FormGroup;
  reviewFormGroup: FormGroup;
  companyForm: FormGroup;
  isLinear = true;
  email = 'abcd@gmail.com';
  @ViewChild('stepper') private myStepper: MatStepper;
  constructor(
    private _snackBar: MatSnackBar,
    private _formBuilder: FormBuilder,
    public datePipe: DatePipe,
    private service: GlobalService,
    private cd: ChangeDetectorRef
  ) {}
  companyList = ['Google', 'Apple', 'Global Foundaries', 'HoneyWell'];
  get: any = [];
  reason = [
    {
      question_str: 'Reason for Absence',
      input_field_type: 'dropdown',
      is_required: 'true',
      maxLength: 10,
      question_values: [],
      answer: '',
    },
    {
      question_str: 'Type of Absence',
      input_field_type: 'radio',
      is_required: 'true',
      maxLength: 10,
      question_values: ['Continous', 'Intermittent'],
      answer: '',
    },
  ];
  dateDetails = [
    {
      id: 1,
      question_str: 'Start Date of Absence',
      input_field_type: 'calendar',
      is_required: 'true',
      maxLength: 10,
      question_values: [],
    },
  ];
  reasonList: any = [];
  typeList = ['Continous', 'Intermittent'];
  company = new FormControl('', Validators.required);
  ngOnInit(): void {
    this.companyForm = this._formBuilder.group({
      company: ['', Validators.required],
    });
    this.reasonFormGroup = this._formBuilder.group({
      'Reason for Absence': ['', Validators.required],
      'Type of Absence': ['', Validators.required],
    });
    this.dateFormGroup = this._formBuilder.group({
      'Start Date of Absence': ['', Validators.required],
    });
    this.detailsFormGroup = this._formBuilder.group({});
    this.addDynamicFieldControls();
    // this.service.getGeneric(environment.getCompany).subscribe(res=>{
    //   this.companyList=res['companies'];
    // })
    this.service.getReasons('Google').subscribe((res) => {
      this.reasonList = res['reasons'];
    });

    this.service.stepEmitter.subscribe((value) => {
      if (value == 0) {
        this.checkValues();
        this.myStepper.selectedIndex = 2;
      }
    });
  }
  addDynamicFieldControls() {
    this.get.forEach((item) => {
      this.detailsFormGroup.addControl(
        item.question_str,
        this._formBuilder.control('', Validators.required)
      );
      item['isReadOnly'] = true;
    });
  }
  pushValues = false;
  companyChange(value) {
    this.service.getReasons(value).subscribe((res) => {
      this.reasonList = res['reasons'];
    });
  }
  checkValues() {
    let sampleJson: any;
    // eslint-disable-next-line prefer-const
    sampleJson = this.reasonFormGroup.value;
    this.service
      .getQuestions(
        sampleJson['Reason for Absence'],
        sampleJson['Type of Absence']
      )
      .subscribe((res) => {
        this.get = res;
        this.addDynamicFieldControls();
        this.cd.detectChanges();
      });
  }

  getDetails() {
    this.pushValues = true;
    this.mapAnswers();
    this.reason[0].answer = this.reasonFormGroup.get(
      this.reason[0].question_str
    ).value;
    this.reason[0].question_values = this.reasonList;
    this.reason[1].answer = this.reasonFormGroup.get(
      this.reason[1].question_str
    ).value;
    this.dateDetails[0]['answer'] = this.datePipe.transform(
      this.dateFormGroup.get(this.dateDetails[0].question_str).value,
      'yyyy-MM-dd'
    );
  }
  mapAnswers() {
    this.get.forEach((item) => {
      if (item.input_field_type == 'calendar') {
        item['answer'] = this.datePipe.transform(
          this.detailsFormGroup.get(item.question_str).value,
          'yyyy-MM-dd'
        );
      } else {
        item['answer'] = this.detailsFormGroup.get(item.question_str).value;
      }
    });
  }
  goBack(stepper: MatStepper) {
    stepper.previous();
  }
  goForward(stepper: MatStepper) {
    if (stepper.selectedIndex == 2) {
      this.getDetails();
    } else if (stepper.selectedIndex == 0) {
      this.checkValues();
    }
    stepper.next();
  }
  saveForm() {
    const postData: any = [];
    this.mapAnswers();
    this.get.forEach((element) => {
      postData.push({
        id: element.id,
        answer: element.answer,
        email: this.email,
      });
    });
    console.log('psotdata', postData);
    this.service.postForm(postData, environment.postAnswers).subscribe(
      (response) => {
        console.log(response);
        if (response) {
          this._snackBar.open('Claim id Created', 'Success', {
            duration: 5000,
            panelClass: ['notif-success'],
          });
        }
      },
      (error) => {
        this._snackBar.open('Something went wrong', 'Failed', {
          duration: 2000,
        });
      }
    );
  }
}

console.log('add UI Items');
console.log('Project end');
