// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  postAnswers: 'http://10.62.10.33/metlife/claims/answers/',
  addQuestions: 'http://10.62.10.33/metlife/claims/questions/',
  postHolidays: 'http://10.62.10.33/metlife/holidays/',
  getCompany: 'http://10.62.10.33/metlife/claims/company/',
  products: 'http://10.62.10.33/metlife/claims/products/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
