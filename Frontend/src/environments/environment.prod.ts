export const environment = {
  production: true,
  postAnswers: 'http://10.62.10.33/metlife/claims/answers/',
  addQuestions: 'http://10.62.10.33/metlife/claims/questions/',
  postHolidays: 'http://10.62.10.33/metlife/holidays/',
  getCompany: 'http://10.62.10.33/metlife/claims/company/',
  products: 'http://10.62.10.33/metlife/claims/products/',
};
