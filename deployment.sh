#!/bin/bash
echo "Backend Deployment"


source metlife-env/bin/activate
cd Backend 

python manage.py collectstatic


sudo systemctl stop metlife-api.service
sudo systemctl daemon-reload

sudo systemctl start metlife-api.service

echo "metlife-api.service has started."

sudo systemctl status metlife-api.service




